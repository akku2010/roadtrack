import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactUsPage } from './contact-us';
import { ReactiveFormsModule } from '@angular/forms';
import { IonPullupModule } from 'ionic-pullup';
import { TranslateModule } from '@ngx-translate/core';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    ContactUsPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactUsPage),
    ReactiveFormsModule,
    SelectSearchableModule,
    IonPullupModule,
    TranslateModule.forChild()
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ContactUsPageModule {}
